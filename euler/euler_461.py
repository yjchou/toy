#!/usr/bin/env python

from cmath import exp, log, pi


def fn(k, n):
    return (exp(float(k) / n) - 1).real


def f_max(n):
    x = log(pi + 4) * n
    return x


def main(N):
    pi_diff = pi+1e-6
    m = int(f_max(N).real + 1)
    fv = [fn(k, N) for k in range(0, m)]

    # return

    result = (100, (0, 0, 0, 0))
    p0 = int(log((pi) / 4 + 1).real * N)
    print m
    for i in range(p0, m):
        print 'progress: {0}% ({1}/{2})'.format(100.0*(i-p0) / (m-p0), i-p0, m-p0)
        w = min(int(log((pi - fv[i]) / 3 + 1).real * N), i)
        for j in range(1, w):
            ww = int(log((pi - fv[i] - fv[j]) / 2 + 1).real * N)
            if fv[i]+fv[j] > pi_diff:
                break
            for k in range(j, ww):
                t = fv[i] + fv[j] + fv[k]
                if t > pi:
                    break
                q = int((log(pi + 1 - t) * N).real)
                diff = min(
                    (abs(pi - t - fv[q]), q), (abs(pi - t - fv[q + 1]), q + 1))
                if (result[0] > diff[0]):
                    result = (diff[0], [i, j, k, diff[1]])
                    print result

    print result
    print sorted(result[1])
    print sum(result[1])


def main2(N):
    pi_diff = pi+1e-6
    m = int(f_max(N).real + 1)
    fv = [fn(k, N) for k in range(0, m+3)]
    limit = [int(log((pi) / x + 1).real * N) for x in range(1, 5)]
    print 'limit =', limit
    lv2 = []
    for i in range(limit[3], m):
        pp = int(log((pi - fv[i]) / 3 + 1).real * N)
        for j in range(1, pp):
            value = fv[i]+fv[j]
            if value < pi_diff:
                lv2.append((value, (i, j)))
            else:
                break
    print 'len(lv2) =', len(lv2)
#    lv2.sort()
    result = (100, (0, 0, 0, 0))
    lv3 = []
    for v, (i, j) in lv2:
        mm = int(log((pi - v) / 2 + 1).real * N)
        for k in range(1, mm):
            vv = v+fv[k]
            t = int((log(pi + 1 - vv) * N).real)
            diff = min(
                (abs(pi - v - fv[t]), t), (abs(pi - v - fv[t + 1]), t + 1))
            if (result[0] > diff[0]):
                result = (diff[0], [i, j, k, diff[1]])
                print result, diff

    #         if t < pi_diff:
    #             lv3.append((t, (i, j, k)))
    #         else:
    #             break
    # print 'len(lv3) =', len(lv3)
#    lv3.sort()
    # lv4 = []
    # result = (100, (0, 0, 0, 0))
    # for v, (i, j, k) in lv3:
    #     t = int((log(pi + 1 - v) * N).real)
    #     diff = min(
    #         (abs(pi - v - fv[t]), t), (abs(pi - v - fv[t + 1]), t + 1))
    #     if (result[0] > diff[0]):
    #         print result, diff
    #         result = (diff[0], [i, j, k, diff[1]])

    # print result
    print result[0]
    print sorted(result[1])
    print sum([x*x for x in result[1]])

if __name__ == '__main__':
    main(200)
    #main2(200)
