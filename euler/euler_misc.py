
import re
import math
import itertools


class Helper(object):

    """ helper functions """

    def __init__(self):
        pass

    def mul(self, lst):
        return reduce(lambda x, y: x * y, lst, 1)

    def primes(self, n):
        """ Returns  a list of primes < n """
        n = int(n)
        sieve = [True] * (n / 2)
        for i in xrange(3, int(n ** 0.5) + 1, 2):
            if sieve[i / 2]:
                sieve[i * i / 2::i] = [False] * ((n - i * i - 1) / (2 * i) + 1)
        return [2] + [2 * i + 1 for i in xrange(1, n / 2) if sieve[i]]

    def prime_gen(self):
        f = []
        x = 2
        while True:
            f.append(x)
            yield x
            while True:
                x += 1
                for p in f:
                    if x % p == 0:
                        break
                else:
                    break

    def factor(self, num, primes=None):
        res = []
        if num == 0:
            return res
        if primes is None:
            primes = self.primes(math.sqrt(num))
        for p in primes:
            if num == 1:
                return res
            while num % p == 0:
                num /= p
                res.append(p)
        if num != 1:
            res.append(num)
        return res

    def divisors(self, num):
        maxn = int(math.sqrt(num))
        res = [1]
        for x in xrange(2, maxn + 1):
            if num % x == 0:
                res.append(x)
                d = num / x
                if d != x:
                    res.append(d)
        res.sort()
        return res


class Euler(object):

    """docstring for Euler"""

    class BreakException(Exception):

        """docstring for BreakException"""

        def __init__(self):
            super(Euler.BreakException, self).__init__()

    def __init__(self):
        super(Euler, self).__init__()
        self.url = 'http://projecteuler.net/problem='
        self.helper = Helper()

    def run(self, num):
        try:
            func = self.__getattribute__('euler_' + str(num))
        except AttributeError:
            print "No such solution ", num
            return
        print 'begin solution', num
        func()
        print 'end of solution', num

    def euler_8(self):
        pattern = """
73167176531330624919225119674426574742355349194934
96983520312774506326239578318016984801869478851843
85861560789112949495459501737958331952853208805511
12540698747158523863050715693290963295227443043557
66896648950445244523161731856403098711121722383113
62229893423380308135336276614282806444486645238749
30358907296290491560440772390713810515859307960866
70172427121883998797908792274921901699720888093776
65727333001053367881220235421809751254540594752243
52584907711670556013604839586446706324415722155397
53697817977846174064955149290862569321978468622482
83972241375657056057490261407972968652414535100474
82166370484403199890008895243450658541227588666881
16427171479924442928230863465674813919123162824586
17866458359124566529476545682848912883142607690042
24219022671055626321111109370544217506941658960408
07198403850962455444362981230987879927244284909188
84580156166097919133875499200524063689912560717606
05886116467109405077541002256983155200055935729725
71636269561882670428252483600823257530420752963450
"""
        pattern = re.sub(r'[^0-9]', '', pattern)
        for inp in [4, 13]:
            maxval = 0
            for idx in range(len(pattern) - inp + 1):
                s = (int(x) for x in pattern[idx:idx + inp])
                res = reduce(lambda x, y: x * y, s, 1)
                maxval = max(maxval, res)
            print maxval

    def euler_9(self):
        """Special Pythagorean triplet"""
        test = [12, 1000]
        for inp in test:
            try:
                for a in range(1, inp + 1):
                    for b in range(a, inp + 1 - a):
                        c = inp - a - b
                        if c ** 2 == a ** 2 + b ** 2:
                            raise Euler.BreakException()
            except Euler.BreakException:
                pass
            print self.helper.mul((a, b, c))

    def euler_10(self):
        """Summation of primes"""
        test = [10, int(2e6)]
        for inp in test:
            print "===", inp, "==="
            print sum(self.helper.primes(inp))

    def euler_12(self):
        """Highly divisible triangular number"""
        test = [5, 500]
        primes = self.helper.primes(4e6)
        for inp in test:
            summ = 0
            for x in xrange(1, 100000):
                summ += x
                uniq = []
                old = [1, 0]
                for p in self.helper.factor(summ, primes):
                    if old[0] == p:
                        old[1] += 1
                    else:
                        uniq.append(old)
                        old = [p, 1]
                uniq.append(old)
                uniq.pop(0)
                prod = 1
                for n, c in uniq:
                    prod *= (c + 1)
                if prod > inp:
                    break
            print summ

    def euler_14(self):
        """Longest Collatz sequence"""
        maxn = int(1e6) + 1
        dp = [0] * maxn
        dp[1] = 1
        maxx = (0, 0)
        for i in xrange(2, maxn):
            if dp[i]:
                continue
            n = i
            c = 0
            while n != 1:
                c += 1
                if n % 2 == 0:
                    n = n / 2
                else:
                    n = 3 * n + 1
                if n < maxn and dp[n]:
                    dp[i] = dp[n] + c
                    maxx = max(maxx, (dp[i], i))
                    break
        print maxx[1]
        # print list(enumerate(dp))

    def euler_15(self):
        """Lattice paths"""
        test = [(2, 2), (20, 20)]
        for r, c in test:
            r, c = r + 1, c + 1
            maze = [[0] * (c + 1) for i in xrange(r + 1)]
            maze[0][1] = 1
            for y in range(1, r + 1):
                for x in range(1, c + 1):
                    maze[y][x] = maze[y - 1][x] + maze[y][x - 1]
            print maze[r][c]

    def euler_16(self):
        """Power digit sum"""
        test = [15, 1000]
        for inp in test:
            n = 2 ** inp
            print sum((int(c) for c in str(n)))

    def euler_18(self, patterns=None):
        """Maximum path sum I"""
        if patterns is None:
            patterns = ["""3
7 4
2 4 6
8 5 9 3""", """75
95 64
17 47 82
18 35 87 10
20 04 82 47 65
19 01 23 75 03 34
88 02 77 73 07 63 67
99 65 04 28 06 16 70 92
41 41 26 56 83 40 80 70 33
41 48 72 33 47 32 37 16 94 29
53 71 44 65 25 43 91 52 97 51 14
70 11 33 28 77 73 17 78 39 68 17 57
91 71 52 38 17 14 91 43 58 50 27 29 48
63 66 04 68 89 53 67 30 73 16 69 87 40 31
04 62 98 27 23 09 70 98 73 93 38 53 60 04 23"""]
        for pat in patterns:
            tri = [[int(c) for c in line.split()] for line in pat.splitlines()]
            for r in range(len(tri) - 2, -1, -1):
                for c in range(len(tri[r])):
                    tri[r][c] += max(tri[r + 1][c], tri[r + 1][c + 1])
            print tri[0][0]

    def euler_67(self):
        """Maximum path sum II"""
        with open('triangle.txt') as f:
            patterns = [f.read()]
            self.euler_18(patterns)

    def euler_20(self):
        """Factorial digit sum"""
        test = [10, 100]
        for inp in test:
            fact = reduce(lambda x, y: x * y, range(3, inp + 1), 2)
            print sum((int(c) for c in str(fact)))

    def euler_22(self):
        """Names scores"""
        with open('names.txt') as f:
            names = [x.replace('"', '') for x in f.read().split(',')]
        names.sort()
        summ = 0
        for i, x in enumerate(names, 1):
            score = sum([ord(c) - ord('A') + 1 for c in x])
            summ += score * i
            if x == 'COLIN':
                print 'COLIN: ', score * i
        print summ

    def euler_23(self):
        """Non-abundant sums"""
        abundant = {}
        for i in xrange(2, 28123):
            dv = self.helper.divisors(i)
            if sum(dv) > i:
                abundant[i] = True
        summ = 0
        ak = sorted(abundant.keys())
        for i in xrange(28123):
            for x in ak:
                if (i - x) in abundant:
                    break
            else:
                summ += i
        print summ

    def euler_25(self):
        """1000-digit Fibonacci number"""
        f = (1, 1)
        c = 2
        while len(str(f[1])) < 1000:
            f1, f2 = f
            f = (f2, f1 + f2)
            c += 1
        print c

    def euler_26(self):
        """Reciprocal cycles"""
        maxm = 10 ** 3000
        res = (0, 0)
        for x in range(2, 1001):
            v = list(str(maxm / x))
            v.reverse()
            v = v[1:-100]
            vlen = len(v)
            for sz in xrange(1, vlen / 2):
                chk = [v[s:s + sz] for s in range(0, vlen / sz * sz, sz)]
                if all([s == chk[0] for s in chk]):
                    res = max(res, (sz, x))
                    break
        print res

    def euler_27(self):
        """Quadratic primes"""
        primes = self.helper.primes(10 ** 5)
        table = set(primes)
        res = (0, 0, 0)
        maxa, maxb = 1000, 1000
        for b in primes:
            if b > maxb:
                break
            for a in range(-b - 1, b):
                if a == 0:
                    continue
                for n in range(b):
                    k = n * (a + n) + b
                    if k not in table:
                        break
                res = max(res, (n, a, b))
        print res[1] * res[2]

    def euler_461(self):
        """Almost Pi"""
        # Long run!!! the second test (1000) takes 8 minues to complete
        # level 2 table size: 72285666 (using 4GB)
        # result 159820276
        import bisect
        pi, exp = math.pi, math.exp
        test = [200]  # , 10000]
        for inp in test:
            ft = []
            for k in itertools.count(0):
                v = exp(float(k) / inp) - 1
                ft.append(v)
                if v > pi:
                    break
            maxn = len(ft)

            table = []
            for a in xrange(1, maxn):
                i = bisect.bisect(ft, pi - ft[a], a)
                fa, a2 = ft[a], a ** 2
                # print 'v2tbl: ', a, i
                table.extend([(fa + ft[b], a2 + b ** 2) for b in range(a, i)])
            table.sort()
            # print 'table(lv1, lv2) size: ', (len(ft), len(table))
            res = (4, 0)
            for i in xrange(1, len(table) / 2 + 1):
                v, t = table[i]
                rem = pi - v
                j = bisect.bisect_left(table, (rem, 0), i)
                if j >= len(table):
                    continue
                res = min(res,
                          (abs(rem - table[j][0]), t + table[j][1]),
                          (abs(rem - table[j - 1][0]), t + table[j - 1][1]))
            print res[1]

if __name__ == '__main__':
    import sys
    problem = 461

    if len(sys.argv) == 2:
        try:
            problem = int(sys.argv[1])
        except:
            print 'Project Euler: Solution lists'
            print sorted((int(s.replace('euler_', ''))
                          for s in Euler.__dict__.keys() if 'euler_' in s))
            sys.exit(0)

    e = Euler()
    e.run(problem)
