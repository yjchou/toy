# README #

It's Polar's toy project. I put some code snippets, handy tools and random thoughts here.

+ euler : [Project Euler](https://projecteuler.net/)
+ shoutcast : web radio with lyric support

## Project Euler ##

Some python solutions for [Project Euler].

## Web radio with lyric ##

Provide LRC format lyric support to specific web radio.

+ Configuration
  Just run radio.py and start streaming http://localhost:5656/ radio

+ Dependency: python libraries
  + requests
  + lxml
