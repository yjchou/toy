#!/usr/bin/env python -u

import requests
import time
import sys
import BaseHTTPServer
from threading import Thread
import ttlrc
import Queue
from Tkinter import *
import tkFont
from ttk import *
from ScrolledText import ScrolledText
import bisect
import logging

URL = 'http://itori.animenfo.com:443/'
HOST = 'localhost'
PORT_NUM = 5656
LOCAL_URL = "http://{0}:{1}/".format(HOST, PORT_NUM)

logger = logging.getLogger("[radio.py]")
logger.setLevel(logging.INFO)
logger.addHandler(logging.StreamHandler())

class Conf(object):

    """global object"""
    pass

C = Conf()
C.radio = None
C.scope = 4
C.default_fontsize = 14
C.enable_textlock = False
C.T = None
C.lyric_delay = None
C.lyric_queue = Queue.Queue()
C.LYRIC_COLOR = ('black', 'blue')
C.LYRIC_CHECK_INTERVAL = 0.5 # in seconds
C.LYRIC_DOWNLOAD_RETRY = 10
C.LYRIC_TIMEDELAY = 6.0  # the lyric timedelay between metadata arrival and song play


class Radio(object):

    def __init__(self, url, chunk=4):
        "docstring"
        self.url = url
        self.chunk = chunk * 1024
        self.pattern = re.compile(r'\(.*\)')

    def go(self, fd=sys.stdout):
        f = requests.get(
            self.url, headers={'Icy-MetaData': 1}, stream=True)
        stream = f.iter_content(self.chunk)

        # modify icy-url to local url
        oldbuf = stream.next()
        targethost = "http://www.animenfo.com/radio"
        localhost = LOCAL_URL.ljust(len(targethost))
        oldbuf = oldbuf.replace(targethost, localhost)
        logger.info("==Radio Header==")
        logger.info(oldbuf[:oldbuf.find('\r\n\r\n')])
        fd.write(oldbuf)

        for buf in stream:
            self.check_meta(oldbuf + buf)
            fd.write(buf)
            oldbuf = buf

    def check_meta(self, bufex):
        meta_idx = bufex.find('StreamTitle')
        if meta_idx != -1 and meta_idx < self.chunk:
            meta_end = bufex.find('\0', meta_idx)
            meta = bufex[meta_idx:meta_end]
            self.meta = [x.split('=')[1][1:-1]
                         for x in meta.split(';') if 'StreamTitle' in x][0]
            self.start_time = time.time()
            logger.info('==Song Metadata==')
            logger.info("Song Meta: {0}\n\tTime: {1}".format(
                self.meta, time.ctime(self.start_time)))
            self.info = self.parse_meta()
            C.lyric_queue.put( (self.info, self.start_time) )

    def parse_meta(self):
        title, artist, album = '', '', ''
        try:
            artist, s = [x.strip() for x in self.meta.split(' - ', 1)]
            title, album = [x.strip() for x in s.rsplit('(', 1)]
            album = album[:-1]
            title = self.pattern.sub('', title)
        except Exception:
            pass
        info = (artist, title, album)
        logger.info("Parsing meta:\n\t" +
             "artist: {0}\n\ttitle: {1}\n\talbum: {2}".format(*info))
        return info


class LyricThread(Thread):

    """manage lyric"""

    def __init__(self, queue):
        super(LyricThread, self).__init__()
        self.tag_pattern = re.compile(r'\[(\d+):(\d+)(.\d+)?\]')
        self.scope = (0, -C.scope, C.scope, 0)
        self.queue = queue
        self.daemon = True

    def run(self):
        while True:
            (artist, title, album), start = self.queue.get()
            # skip AnimeNfo advertisement
            if not artist == 'www.AnimeNfo.com/radio':
                start += C.LYRIC_TIMEDELAY
                lyric = self.get_lyric(artist, title, album)
                timestamp = self.lrc_parser(lyric)
                self.update_guitext(lyric, timestamp)
                if timestamp:
                    logger.debug("==Timestamp==")
                    logger.debug(timestamp)
                    self.update_timestamp(start, timestamp)

    def get_lyric(self, artist, title, album):
        res = ttlrc.query(artist, title)
        lyrics = None
        text = "TITLE: {1}\nARTIST: {0}  , ALBUM: {2}\n\n".format(artist, title, album)
        if len(res):
            for x in range(1, C.LYRIC_DOWNLOAD_RETRY):
                lyrics = ttlrc.get(*res[0][1:])
                if "Search ID or Code error" not in lyrics:
                    break
                logger.debug("get lyric fail, retry count {0}".format(x))
                time.sleep(1)
            else:
                logger.warn("found lyric in database but cannot retrieve it!")
                lyrics = None
        if not lyrics:
            lyrics = "Cannot find lyric !!"
        text += lyrics
        if lyrics:
            logger.debug('==Lyric data==')
            for i, line in enumerate(text.splitlines(), 1):
                logger.debug("%3d : %s", i, line)
        return text

    def update_guitext(self, lyric, timestamp):
        lyric_notag = self.tag_pattern.sub('', lyric)
        lock_textarea(False)
        C.T.delete('1.0', END)
        C.T.insert('1.0', lyric_notag)
        C.lyric_delay.set('0')
        for t, lineno in timestamp:
            row = str(lineno)
            C.T.tag_add("tag_"+row, row+".0", row+".1000")
        lock_textarea(True)

    def lrc_parser(self, lrc):
        tag_line = []
        for lineno, line in enumerate(lrc.splitlines(), 1):
            for tag in [ (int(i[0]), int(i[1]), i[2]) for i in self.tag_pattern.findall(line)]:
                timestamp = tag[0]*60+tag[1]
                if i[2]:
                    timestamp += int(tag[2][1:])*0.01
                tag_line.append((timestamp, lineno))
        tag_line.sort()
        return tag_line

    def playtime(self):
        try:
            d = int(C.lyric_delay.get())
        except ValueError:
            d = 0
        return time.time() - C.lyric_start + d

    def update_timestamp(self, start, timestamp):
        def color_line(lineno, fg, font):
            C.T.tag_config("tag_" + str(lineno), foreground=fg, font=font)

        C.lyric_start = start
        sel = (0.0, 1)
        default_color, play_color = C.LYRIC_COLOR
        timestop = [i[0] for i in timestamp]
        while self.queue.empty():
            play = self.playtime()
            if timestop[0] <= play <= timestop[-1] + 3:
                idx = bisect.bisect(timestop, play)
                try:
                    newsel = timestamp[idx]
                except IndexError:
                    newsel = sel
                if newsel[1] != sel[1]:
                    color_line(sel[1], default_color, C.fontstyle)
                    color_line(newsel[1], play_color, C.bigfontstyle)
                    sel = newsel
                    # scroll textarea if necessary
                    for i in self.scope:
                        C.T.see(str(sel[1]+ i) + '.0')
            wait = min(sel[0] - play, C.LYRIC_CHECK_INTERVAL)
            time.sleep(max(wait, 0))
        color_line(sel[1], default_color, C.fontstyle)


class ManInMiddle(BaseHTTPServer.BaseHTTPRequestHandler):

    def do_GET(self):
        C.radio.go(self.wfile)


def start_httpd():
    httpd = BaseHTTPServer.HTTPServer((HOST, PORT_NUM), ManInMiddle)
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        raise
    http.server_close()

def lock_textarea(flag):
    if C.enable_textlock:
        C.T['state'] = 'disable' if flag else 'normal'

def gui_text():
    def OnFontChange():
        fontstyle['size'] = FontControl.get()

    def lyric_button(diff):
        def f():
            try:
                d = int(C.lyric_delay.get())
            except ValueError:
                d = 0
            C.lyric_delay.set(str(d+diff))
        return f

    root = Tk()
    root.title('WebRadio: ' + C.url)
    # create variable
    C.lyric_delay = StringVar(value='0')
    fontstyle = tkFont.Font(size=C.default_fontsize)
    bigfontstyle = tkFont.Font(size=22)
    font_size = IntVar(value=fontstyle['size'])

    # create widget
    cmdbar = LabelFrame(root, text='Command')
    Label(cmdbar, text='Font size: ').pack(side='left', )
    FontControl = Spinbox(cmdbar, textvariable=font_size,
                          width=4, justify='center', from_=8, to=18, command=OnFontChange)
    # widget: lyric sync timing control
    delay_frame = Frame(cmdbar)
    Label(delay_frame, text='Lyric timing offset: ').pack(side='left')
    Button(delay_frame, text='^', width=1,
           command=lyric_button(-1)).pack(side='left')
    Entry(delay_frame, width=4, justify='center',
          textvariable=C.lyric_delay).pack(side='left')
    Button(delay_frame, text='v', width=1,
           command=lyric_button(1)).pack(side='left')
    # widget: lyric frame
    lyric = LabelFrame(root, text='Lyric', borderwidth=4)
    T = ScrolledText(lyric, height=40, width=50,
                     font=fontstyle)
    T.insert(END, "just connect " + LOCAL_URL)
    lock_textarea(True)

    # create packing
    cmdbar.pack(side='top', fill=X, padx=1, pady=1)
    lyric.pack(side='bottom', expand=YES, fill=BOTH)
    T.pack(side='bottom', expand=YES, fill=BOTH)
    FontControl.pack(side='left')
    delay_frame.pack(side='right')

    C.fontstyle = fontstyle
    C.bigfontstyle = bigfontstyle
    C.T = T


def parse_arguments():
    import argparse
    parser = argparse.ArgumentParser(
        description='Intercept web radio and add lyric support')
    parser.add_argument('url', nargs='?', default=URL,
                        help='the url of web radio. Default: ' + URL)
    parser.add_argument('-a', '--app',
                        help='Launch app(iTunes, VOX, ...) to listen radio.')
    parser.add_argument('-v', '--verbose', action='store_true',
                        help='print verbose messages')

    return parser.parse_args()

if __name__ == '__main__':
    args = parse_arguments()
    C.url = args.url
    C.radio = Radio(C.url, 4)
    C.lyric_queue = Queue.Queue()
    threads = [Thread(target=x) for x in [start_httpd]]
    threads.append(LyricThread(C.lyric_queue))

    for t in threads:
        t.daemon = True
        t.start()

    print "just connect the url:\n" + LOCAL_URL
    gui_text()
    if args.verbose:
        logger.setLevel(logging.DEBUG)
    if args.app:
        import subprocess
        subprocess.call(('/usr/bin/open', '-a', args.app, LOCAL_URL))
    mainloop()


def test_get_lyric():
    artist = "Onitsuka Chihiro"
    title = "Castle Imitation"
    print get_lyric(artist, title)
